# Scraper 1.0.0
## David Smith <dasmith91@gmail.com>

To run the scraper:

1. Install all dependencies by running ```pip install -e .``` in the root directory of the project dir.
2. Run the project by running ```python scraper/__init__.py --url <url_here>```.
3. The scraper should extract the page contents and print json to stdout. (Good for piping into files!)

To run the tests:

1. Install all dependencies by running ```pip install -e .``` in the root directory of the project dir.
2. Run ```py.test``` or for more verbose output add the ```-v``` flag.

To modify logging options, edit ```scraper/scraper.json```.
