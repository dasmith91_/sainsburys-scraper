# -*- coding: utf-8 -*-
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import pytest
import requests

from scraper import exceptions
from scraper import util


def test_fetch_single_element():
    result = util.fetch(['https://www.google.com'])
    assert len(result) == 1
    assert isinstance(result[0], unicode)


def test_fetch_multiple_element():
    result = util.fetch(['https://www.google.com', 'https://www.yahoo.com', 'https://www.sainsburys.co.uk'])
    assert len(result) == 3

    for item in result:
        assert isinstance(item, unicode)


def test_fetch_no_schema():
    with pytest.raises(exceptions.MalformedURLError):
        util.fetch(['allseeinggoatwizard.com'])


def test_fetch_bad_schema():
    with pytest.raises(exceptions.MalformedURLError):
        util.fetch(['bloop://google.com'])


def test_fetch_bad_url():
    with pytest.raises(requests.HTTPError):
        util.fetch(['https://www.google.com/thisurltotallydoesntexist'])
