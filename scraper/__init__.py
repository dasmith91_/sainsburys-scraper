# -*- coding: utf-8 -*-
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
import argparse
import json

from bs4 import BeautifulSoup

from scraper import util


def process_url(url):
    """
    Process a Sainsbury's item list page, scraping all elements and it's children from the page. Format the results and
    calculate the total of all the unit prices. The output can then be jsonified.

    :param url: The URL to scrape.
    :type url: unicode
    :return: dict
    """
    output = []  # Final return value.

    parent = util.fetch([url])[0]  # Fetch uses a list to keep the API consistent.
    parent = BeautifulSoup(parent)

    products = parent.find_all('div', attrs={'class': 'product'})

    children = util.fetch([product.h3.a.get('href') for product in products])
    file_sizes = [len(child) / 1000 for child in children]
    children = [BeautifulSoup(child) for child in children]  # Now convert the children to BS4 Objects

    for product, child, file_size in zip(products, children, file_sizes):
        # Grab title and remove whitespace.
        title = product.h3.text.strip()

        # Grab price and clean up by removing £ and /unit
        price = product.find('p', attrs={'class', 'pricePerUnit'}).text.strip()[1:-5]

        desc = child.find('div', attrs={'class': 'productText'}).find_all('p')  # Get Description
        desc = [p.text.strip() for p in desc]  # Clean up text
        desc = [p for p in desc if p != '']  # Exclude any empty strings.

        output.append({
            'title': title,
            'unit_price': float(price),
            'description': '\n'.join(desc),  # Add some new line formatting
            'size': '{:.2f}kb'.format(file_size)  # Limit to 2dp
        })

    return {
        'results': output,
        'total': sum([item['unit_price'] for item in output])
    }

def main(url):
    result = process_url(url)
    print(json.dumps(result, indent=4))


if __name__ == '__main__':
    parser = argparse.ArgumentParser("An example web scraper for the Sainsbury's technical test.")
    parser.add_argument('--url', type=unicode, required=True)
    parser.add_argument('--log', type=unicode, default='scraper.json')
    args = parser.parse_args()

    util.setup_logging(args.log)
    main(args.url)
