# -*- coding: utf-8 -*-
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals


class BaseScraperException(Exception):
    pass


class MalformedURLError(BaseScraperException):
    pass
