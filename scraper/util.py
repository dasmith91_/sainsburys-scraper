# -*- coding: utf-8 -*-
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import json
import logging
from logging.config import dictConfig

import grequests

from scraper import exceptions

logger = logging.getLogger('scraper')


def setup_logging(config_file):
    try:
        with open(config_file) as f:
            dictConfig(json.load(f))
    except IOError:
        print('Failed to find config at {}'.format(config_file))


def fetch(urls):
    """
    :param urls: A list of urls to be requested. These are processed in parallel.
    :type urls: [unicode]

    :rtype : [unicode]
    """
    logger.info('Fetching {} url(s)'.format(len(urls)))
    results = grequests.map([grequests.get(url) for url in urls])  # Nice and quick.

    for result in results:
        if result is None:
            raise exceptions.MalformedURLError  # grequests returns a null object on error.
        result.raise_for_status()  # 'splodes on anything that isn't a 2xx status.

    return [result.text for result in results]
