#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from setuptools import setup

with open('requirements.txt') as f:
    requirements = [r.strip() for r in f.readlines()]

setup(
    name='scraper',
    version='1.0.0',
    packages=['scraper'],
    url='https://www.davidsmith.guru',
    license='MIT',
    author='David Smith',
    author_email='dasmith91@gmail.com',
    description='An example web scraper for the Sainsbury\'s technical test.',
    install_requires=requirements
)
